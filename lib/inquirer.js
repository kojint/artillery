const inquirer    = require('inquirer');

module.exports = {

  /* askGithubCredentials: () => {
    const questions = [
      {
        name: 'username',
        type: 'input',
        message: 'Enter your Github username or e-mail address:',
        validate: function( value ) {
          if (value.length) {
            return true;
          } else {
            return 'Please enter your username or e-mail address.';
          }
        }
      },
      {
        name: 'password',
        type: 'password',
        message: 'Enter your password:',
        validate: function(value) {
          if (value.length) {
            return true;
          } else {
            return 'Please enter your password.';
          }
        }
      }
    ];
    return inquirer.prompt(questions);
  },

  askRegeneratedToken: () => {
    const questions = [
      {
        name: 'token',
        type: 'input',
        message: 'Enter your new regenerated token:',
        validate: function( value ) {
          if (value.length) {
            return true;
          } else {
            return 'Please enter your new regenerated token:.';
          }
        }
      }
    ];
    return inquirer.prompt(questions);
  },

  askRepoDetails: () => {
    const argv = require('minimist')(process.argv.slice(2));

    const questions = [
      {
        type: 'input',
        name: 'name',
        message: 'Enter a name for the repository:',
        default: argv._[0] || files.getCurrentDirectoryBase(),
        validate: function( value ) {
          if (value.length) {
            return true;
          } else {
            return 'Please enter a name for the repository.';
          }
        }
      },
      {
        type: 'input',
        name: 'description',
        default: argv._[1] || null,
        message: 'Optionally enter a description of the repository:'
      },
      {
        type: 'list',
        name: 'visibility',
        message: 'Public or private:',
        choices: [ 'public', 'private' ],
        default: 'public'
      }
    ];
    return inquirer.prompt(questions);
  },

  askIgnoreFiles: (filelist) => {
    const questions = [
      {
        type: 'checkbox',
        name: 'ignore',
        message: 'Select the files and/or folders you wish to ignore:',
        choices: filelist,
        default: ['node_modules', 'bower_components']
      }
    ];
    return inquirer.prompt(questions);
  }, */

askProjectDetails: () => {
  const questions = [
    {
      type: 'input',
      name: 'project_name',
      message: 'Enter a name for the project:'
    },
    {
      type: 'input',
      name: 'project_description',
      message: 'Enter a description for the project:'
    },
    {
      type: 'list',
      name: 'project_type',
      message: 'What kind of project is it?',
      choices: ["Angular", "Craft", "WordPress"],
    },
    {
      type: 'list',
      name: 'project_db',
      message: 'Will the project need a database?',
      choices: ["Yes", "No"],
    },
  ];
  return inquirer.prompt(questions);
},
askDatabaseDetails: () => {
  const questions = [
    {
      type: 'input',
      name: 'db_host',
      message: 'Enter a hostname of the database:'
    },
    {
      type: 'input',
      name: 'db_schema',
      message: 'Enter a schema name for the database:'
    },
    {
      type: 'input',
      name: 'db_user',
      message: 'Enter a database username:'
    },
    {
      type: 'input',
      name: 'db_pass',
      message: 'Enter a database password:'
    },
  ];
  return inquirer.prompt(questions);
},
askInitialise: () => {
  const questions = [
    {
      type: 'list',
      name: 'project_init',
      message: 'Initialise the project? Choosing No will abort:',
      choices: ["Yes", "No"],
    },
    {
      type: 'list',
      name: 'git_init',
      message: 'What Git service should be set?',
      choices: ["Bitbucket", "Giithub", "None"],
    },
    {
      type: 'input',
      name: 'git_remote',
      message: 'Enter your git remote url:'
    },
  ];
  return inquirer.prompt(questions);
},
askCi: () => {
  const questions = [
    {
      type: 'list',
      name: 'ci_init',
      message: 'Setup Cavalry CI',
      choices: ["Yes", "No"],
    },
  ];
  return inquirer.prompt(questions);
},

  // STEPS

  /* // Initiate Project
    Ask project_name
    Ask project_description
    Ask project_type
    Ask if project needs db - y/n
      If y:
        Ask db_host
        Ask db_schema
        Ask db_user
        Ask db_pass
      If n: continue
    Ask to initialise - y/n
      If y:
        If project_type === Angular: pull and set env
        If project_type === Craft: pull and set env
        If project_type === WordPress: pull and set env
      If n: complete/die
    Create project Readme.md
    
  // Create Bitbucket/Git Repo
    Ask to initialise Git - y/n
    Ask to setup git_source - Bitbucket(b)/Github(g)
      If git_source === Bitbucket: login and create repo
      If git_source === Github: login and create repo
    
  // Setup Cavalry CI
    Ask to setup Cavalry CI - y/n
      If y: copy files and configure
      If n: complete/die */

};
