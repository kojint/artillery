#!/usr/bin/env node

const chalk = require('chalk');
const clear = require('clear');
const figlet = require('figlet');

const inquirer = require("inquirer");
//const shell = require("shelljs");

const logic    = require('./lib/logic');

clear();

const init = () => {
    console.log(
        chalk.red(
            figlet.textSync('artillery', {
                font: "Larry 3D",
                horizontalLayout: 'fitted'
            })
        )
    );
    console.log(
        chalk.white.bold("*** Setup and tooling configuration for dev projects ***")
    );
    console.log(
        chalk.white.bold("")
    );
}

const run = async () => {

    // Show Banner
    init();

    // Initiate Project
    await logic.setProjectDetails();

    // Create Bitbucket/Git Repo
    await logic.setInitialise();
    
    // Setup Cavalry CI
    await logic.setCI();

    // Show success message

};

run();