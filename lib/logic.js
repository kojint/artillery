const Configstore = require('configstore');
const pkg         = require('../package.json');
const _           = require('lodash');
const CLI         = require('clui');
const Spinner     = CLI.Spinner;
const chalk       = require('chalk');
const shell       = require('shelljs');

const inquirer    = require('./inquirer');

const conf = new Configstore(pkg.name);

module.exports = {

  setProjectDetails: async () => {
    // Main project information
    const project_details = await inquirer.askProjectDetails();
    console.log(project_details);

    // Database information
    if (project_details.project_db === "Yes") {
        const database_details = await inquirer.askDatabaseDetails();
        console.log(database_details);
    }
  },
  setInitialise: async () => {
    // Initialise project
    const set_init = await inquirer.askInitialise();
    console.log(set_init);

    if (set_init.project_init === "Yes") {
        // logic to run initialisation
        console.log('Initialising your project...');
        if (setProjectDetails.project_details.project_type === "Angular") {
            // run angular init
            // shell.exec('git init')
        }
        else if (setProjectDetails.project_details.project_type === "Craft") {
            // run angular init
        }
        else if (setProjectDetails.project_details.project_type === "WordPress") {
            // run angular init
        }
    }
    else if (set_init.project_init === "No") {
        console.log('Your aborted project details were: '+setProjectDetails.project_details);
    }

    // Initialise Git
    if (set_init.git_init === "Bitbucket") {
        // console.log('Bitbucket logic goes here');
        await shell.exec('git init');
        await shell.exec('git add --all');
        await shell.exec('git commit -m "Initial Commit"');
        await shell.exec('git remote add origin '+ set_init.git_remote);
        await shell.exec('git push -u origin master');
    }
    if (set_init.git_init === "Github") {
        // console.log('Github logic goes here');
        shell.exec('git init')
    }
  },
  setCI: async () => {
    // Initialise CI
    const project_ci = await inquirer.askCi();
    console.log(project_ci);

    if (project_ci.ci_init === "Yes") {
        console.log('Logic to copy/pull CI goes here');
    }
  },

};
