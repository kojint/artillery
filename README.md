> ```
>
>               /\ \__  __ /\_ \  /\_ \
>   __     _ __ \ \ ,_\/\_\\//\ \ \//\ \       __   _ __   __  __
> /'__`\  /\`'__\\ \ \/\/\ \ \ \ \  \ \ \    /'__`\/\`'__\/\ \/\ \
>/\ \L\.\_\ \ \/  \ \ \_\ \ \ \_\ \_ \_\ \_ /\  __/\ \ \/ \ \ \_\ \
>\ \__/.\_\\ \_\   \ \__\\ \_\/\____\/\____\\ \____\\ \_\  \/`____ \
> \/__/\/_/ \/_/    \/__/ \/_/\/____/\/____/ \/____/ \/_/   `/___/> \
>                                                              /\___/
>                                                              \/__/
>
> ```

# Welcome to Artillery

Artillery is a [Node.js](https://nodejs.org/) powered CLI to help with the setup and tooling configuration for dev projects. Simple, dev-serviced weapons for the Cavalry.

## Instructions

To run Artillery, do the following: 

1. Download the Artillery repo
2. Cd into Artillery and run `npm install` to install dependencies
3. Run `npm link` to link the bin command
4. Run `artillery` from anywhere globally